﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class ChatController : MonoBehaviour
	{

		public GameObject ChatView;
		public GameObject OptionView;
		public InputField IpAddres;
		public GameObject TCPChatController;
		public Text TextToSend;

		void Start()
		{
			IpAddres.text = IpAddres.placeholder.GetComponent<Text>().text;
			OptionView.SetActive(true);
			ChatView.SetActive(false);
		}

		void Update()
		{
			if (Input.GetKeyDown(KeyCode.Return))
			{
				SendMessage();
			}
		}

		public void StartChat(bool isServer)
		{
			string ipAddresToConnect = isServer ? null : IpAddres.text;
			TCPChatController.GetComponent<TCPChat>().Init(isServer, ipAddresToConnect);

			OptionView.SetActive(false);
			ChatView.SetActive(true);
		}

		public void SendMessage()
		{
			TCPChat.Instance.Send(TextToSend.text);
			TextToSend.GetComponentInParent<InputField>().text = "";
		}
	}
}