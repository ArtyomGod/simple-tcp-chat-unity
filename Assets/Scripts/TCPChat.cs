﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class TCPChat : MonoBehaviour
	{
		public static int Name;

		public static TCPChat Instance;

		public bool IsServer;

		public IPAddress ServerIp;

		readonly List<TcpConnectedClient> _clientList = new List<TcpConnectedClient>();

		public static string MessageToDisplay;

		public Text TextToDisplay;

		public Text HashCode;

		private TcpListener _listener;
		
		public void Init(bool isServer, string ipAddres)
		{
			Instance = this;
			this.IsServer = isServer;
			if (ipAddres != null)
			{
				ServerIp = IPAddress.Parse(ipAddres);
			}
			
			if (ServerIp == null)
			{
				this.IsServer = true;
				_listener = new TcpListener(IPAddress.Any, Globals.Port);
				_listener.Start();
				_listener.BeginAcceptTcpClient(OnServerConnect, null);

				Name = _listener.Server.GetHashCode();
			}
			else
			{
				TcpClient client = new TcpClient();
				TcpConnectedClient connectedClient = new TcpConnectedClient(client);
				_clientList.Add(connectedClient);
				client.BeginConnect(ServerIp, Globals.Port, (ar) => connectedClient.EndConnect(ar), null);

				Name = client.Client.GetHashCode();
			}

			HashCode.text = Name.ToString();
		}

		protected void OnApplicationQuit()
		{
			if (_listener != null)
			{
				_listener.Stop();
			}
			foreach (TcpConnectedClient client in _clientList)
			{
				client.Close();
			}
		}

		protected void Update()
		{
			TextToDisplay.text = MessageToDisplay;
		}
		
		void OnServerConnect(IAsyncResult ar)
		{
			TcpClient tcpClient = _listener.EndAcceptTcpClient(ar);
			_clientList.Add(new TcpConnectedClient(tcpClient));

			_listener.BeginAcceptTcpClient(OnServerConnect, null);
		}

		public void OnDisconnect(TcpConnectedClient client)
		{
			_clientList.Remove(client);
		}

		internal void Send(string message)
		{
			BroadcastChatMessage(message);

			if (IsServer)
			{
				MessageToDisplay += String.Format("{0} {1}: {2}{3}", DateTime.Now, Name, message, Environment.NewLine);
			}
		}

		internal static void BroadcastChatMessage(string message)
		{
			BroadcastChatMessage(message, Name);
		}

		internal static void BroadcastChatMessage(string message, int hostName)
		{
			foreach (TcpConnectedClient client in Instance._clientList)
			{
				client.Send(message, hostName);
			}
		}
	}
}