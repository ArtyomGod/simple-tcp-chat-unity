﻿using System;
using System.Net.Sockets;

namespace Assets.Scripts
{
	public class TcpConnectedClient
	{
		readonly TcpClient _connection;

		readonly byte[] _readBuffer = new byte[5000];

		NetworkStream Stream
		{
			get { return _connection.GetStream(); }
		}
		
		public TcpConnectedClient(TcpClient tcpClient)
		{
			this._connection = tcpClient;
			this._connection.NoDelay = true;
			if (TCPChat.Instance.IsServer)
			{
				Stream.BeginRead(_readBuffer, 0, _readBuffer.Length, OnRead, null);
			}
		}

		internal void Close()
		{
			_connection.Close();
		}

		void OnRead(IAsyncResult ar)
		{
			int length = Stream.EndRead(ar);
			if (length <= 0)
			{
				TCPChat.Instance.OnDisconnect(this);
				return;
			}
		
			ushort messageSize = BitConverter.ToUInt16(_readBuffer, 0);
			int authtor = BitConverter.ToInt32(_readBuffer, 2);
			string newMessage = System.Text.Encoding.UTF8.GetString(_readBuffer, 6, messageSize);

			TCPChat.MessageToDisplay += String.Format("{0} {1}: {2}{3}", DateTime.Now, authtor, newMessage, Environment.NewLine);

			if (TCPChat.Instance.IsServer)
			{
				TCPChat.BroadcastChatMessage(newMessage, authtor);
			}

			Stream.BeginRead(_readBuffer, 0, _readBuffer.Length, OnRead, null);
		}

		internal void EndConnect(IAsyncResult ar)
		{
			_connection.EndConnect(ar);

			Stream.BeginRead(_readBuffer, 0, _readBuffer.Length, OnRead, null);
		}

		internal void Send(string message, int authtor)
		{
			byte[] buffer = PackMessage(message, authtor);
		
			Stream.Write(buffer, 0, buffer.Length);
		}

		internal byte[] PackMessage(string message, int authtor)
		{
			byte[] authtorBytes = BitConverter.GetBytes(authtor);
			byte[] messageBytes = System.Text.Encoding.UTF8.GetBytes(message);
			byte[] lengthOfMessage = BitConverter.GetBytes((ushort)messageBytes.Length);
			byte[] result = new byte[messageBytes.Length + authtorBytes.Length + lengthOfMessage.Length];

			lengthOfMessage.CopyTo(result, 0);
			authtorBytes.CopyTo(result, lengthOfMessage.Length);
			messageBytes.CopyTo(result, authtorBytes.Length + lengthOfMessage.Length);

			return result;
		}
	}
}